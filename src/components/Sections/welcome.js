import './css/welcome.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { Columns, Container, Tile, Heading, Image, Box, Button } from 'react-bulma-components/full';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown, faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons';
import { faInstagram, faLinkedinIn, faBitbucket } from '@fortawesome/free-brands-svg-icons';


export class Welcome extends React.Component {
    constructor() {
        super();
        this.state = {}
    }

    render() {
        return (
            <Container>
              <div className="welcomeContainer">
              <section className="logoContainer">
                <Columns>
                  <Columns.Column size={5} offset={1}>
                    <div className="logoText"><b>GD</b><br />
                    <div className="logoGradient">#Designing<br />
                    #Prototyping<br />
                    #Learning</div></div>
                  </Columns.Column>
                  <Columns.Column size={5} offset={1}>
                  <div className="welcomeSkillsPanel">
                      <Button size="small" color="info">BALSAMIQ</Button>
                      <Button size="small" color="light">GIT</Button>
                      <Button size="small" color="info">PHANTOMJS</Button>
                      <Button size="small" color="light">JENKINS</Button>
                      <Button size="small" color="success">SQL</Button>
                      <Button size="small" color="warning">FINANCIAL</Button>
                      <Button size="small" color="info">HTML5</Button>
                      <Button size="small" color="info">CSS3</Button>
                      <Button size="small" color="info">REACT</Button>
                      <Button size="small" color="success">ASE</Button>
                      <Button size="small" color="info">ES6</Button>
                      <Button size="small" color="light">WEBPACK</Button>
                      <Button size="small" color="warning">UNIX</Button>
                      <Button size="small" color="light">NPM</Button>
                      <Button size="small" color="info">D3</Button>
                      <Button size="small" color="primary">CLOUD</Button>
                      <Button size="small" color="primary">SELENIUM</Button>
                      <Button size="small" color="primary">SPECFLOW</Button>
                      <Button size="small" color="danger">UNIX</Button>
                      <Button size="small" color="dark">C# WPF</Button>
                      <Button size="small" color="light">VB.NET</Button>
                      <Button size="small" color="dark">ABAP</Button>
                      <Button size="small" color="warning">UNIX</Button>
                      <Button size="small" color="success">SQL</Button>
                      <Button size="small" color="primary">JQUERY</Button>
                      <Button size="small" color="info">CSS</Button>
                      <Button size="small" color="info">PHOTOSHOP</Button>
                      <Button size="small" color="info">QUICKTEST PRO</Button>
                      <Button size="small" color="success">SAP</Button>
                      <Button size="small" color="success">HPSM</Button>
                      <Button size="small" color="success">ALM</Button>
                      <Button size="small" color="success">SIX SIGMA</Button>
                      <Button size="small" color="primary">SHAREPOINT</Button>
                      <Button size="small" color="warning">UNIX</Button>
                      <Button size="small" color="success">SQL</Button>
                      <Button size="small" color="primary">JQUERY</Button>
                      <Button size="small" color="info">CSS</Button>
                      <Button size="small" color="info">PHOTOSHOP</Button>
                      <Button size="small" color="info">QUICKTEST PRO</Button>
                      <Button size="small" color="success">SAP</Button>
                      <Button size="small" color="success">HPSM</Button>
                      <Button size="small" color="success">ALM</Button>
                      <Button size="small" color="success">SIX SIGMA</Button>
                      <Button size="small" color="primary">SHAREPOINT</Button>
                      <Button size="small" color="success">SAP</Button>
                      <Button size="small" color="success">HPSM</Button>
                      <Button size="small" color="success">ALM</Button>
                      <Button size="small" color="success">SIX SIGMA</Button>
                      <Button size="small" color="primary">SHAREPOINT</Button>
                      <Button size="small" color="success">ALM</Button>
                      <Button size="small" color="success">SIX SIGMA</Button>
                      <Button size="small" color="primary">SHAREPOINT</Button>
                      <Button size="small" color="warning">UNIX</Button>
                      <Button size="small" color="success">SQL</Button>
                      <Button size="small" color="primary">JQUERY</Button>
                    </div>
                  </Columns.Column>
                </Columns>
              </section>
              {/* <Columns>
                <Columns.Column size={1} className="leftFloat" offset={3}>
                  <a href="https://www.linkedin.com/in/grant-downie-a11a7231/"><FontAwesomeIcon icon={faLinkedinIn} size="2x" alt="BitBucket Profile For GKD" /></a>
                </Columns.Column>
                <Columns.Column size={1} className="leftFloat">
                  <a href="https://www.linkedin.com/in/grant-downie-a11a7231/"><FontAwesomeIcon icon={faInstagram} size="2x" alt="BitBucket Profile For GKD" /></a>
                </Columns.Column>
                <Columns.Column size={1} className="leftFloat">
                  <a href="https://www.linkedin.com/in/grant-downie-a11a7231/"><FontAwesomeIcon icon={faBitbucket} size="2x" alt="BitBucket Profile For GKD" /></a>
                </Columns.Column>
              </Columns> */}
          </div>
          {/* <Columns.Column size={12} offset={5}>
                  <FontAwesomeIcon icon={faAngleDown} size="3x" alt="BitBucket Profile For GKD" />
          </Columns.Column> */}
          </Container>
        )
    }
}
