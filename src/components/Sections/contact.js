import React from 'react';
import ReactDOM from 'react-dom';
import './css/contact.css';
import { Section, Hero, Columns, Footer, Container, Content, Heading, Button } from 'react-bulma-components/full';
import mainAv from'../../../images/av.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInstagram, faLinkedinIn, faBitbucket } from '@fortawesome/free-brands-svg-icons';

export class Contact extends React.Component {
    constructor() {
        super();
        this.state = {}
    }

    render() {
        return (
                    <Content>
                        <Container>
                            <Section id="Contact" style={{ background: '#fafafa', marginBottom:'10vh', border:'1px solid #eee'}}>
                                <Columns>
                                <Columns.Column size={10}>
                                    <form id="contactForm"><input color="danger" type="email" placeholder="Want to collaborate? Drop me your email..." /></form>
                                </Columns.Column>
                                <Columns.Column size={1}>
                                <Button size="medium" color="info">Submit</Button>
                                </Columns.Column>
                                </Columns>
                            </Section>
                        </Container>
                    </Content>
        )
    }
}
