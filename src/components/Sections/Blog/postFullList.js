import '../css/welcome.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { Columns, Container, Tile, Heading, Image, Box, Card, Media, Content, Button, Dropdown } from 'react-bulma-components/full';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { } from '@fortawesome/fontawesome-svg-core';
import { faAngleDown, faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons'
var moment = require('moment');

var rowArray = [];

export class PostFullList extends React.Component {
    constructor() {
        super();
        this.state = { categorySelected:'All'}
        this.createArticlePlacehoder = this.createArticlePlacehoder.bind(this);
        this.openArticle = this.openArticle.bind(this);
        this.handleReceivedPosts = this.handleReceivedPosts.bind(this);
        this.handleReceivedCategories = this.handleReceivedCategories.bind(this);
        this.onCategoryChange = this.onCategoryChange.bind(this);
    }

    openArticle(e) {
      this.props.openPost(e.target.id)
    }

    createArticlePlacehoder(id, title, date, bgIMG) {
      let articlePlaceholder = <Columns.Column key={id} size={3}>
                                <Card className="postListItem" style={{backgroundImage: "url(" + bgIMG + ")"}}>
                                  <Card.Content>
                                  <Content>
                                    <time hidden dateTime={date}>{date}</time>
                                  </Content>
                                    <Media>
                                      <Media.Item><Heading style={{textAlign:'center', background:'rgba(247, 239, 237, 0.9)'}}><a id={id} onClick={ (e) => this.openArticle(e) }>{title}</a></Heading></Media.Item>
                                    </Media>
                                  </Card.Content>
                                </Card></Columns.Column>
      rowArray.push(articlePlaceholder);
      this.setState({articleCollection:rowArray})
    }

    handleReceivedPosts() {
      if(this.props.postCollection.length == 0) {
        this.setState({articleCollection:<h1>No Posts? Hmmm. There should be. Contact me.</h1>})
      } else {
        for(let i = 0; i < this.props.postCollection.length; i++) {
          console.log(this.props.postCollection);
          this.createArticlePlacehoder(
              this.props.postCollection[i].id,
              this.props.postCollection[i].title.rendered,
              moment(this.props.postCollection[i].date).format("Do MMM YYYY"),
              this.props.postCollection[i]._embedded['wp:featuredmedia']['0'].source_url
          );
        }
      }
    }

    handleReceivedCategories() {
      let categories = this.props.postCategories;
      let catArr = [];
      for(var i = 0; i < categories.length; i++) {
        console.log(this.props.postCategories[i])
            let categoryDropdownPlaceholder = <Dropdown.Item key={categories[i].id} value={categories[i].name}>{categories[i].name}</Dropdown.Item>;
            catArr.push(categoryDropdownPlaceholder);
      }
      catArr.sort();
      this.setState({categoryCollection:catArr})
    }

    componentWillMount() {
      this.handleReceivedPosts();
      this.handleReceivedCategories();
    }

    onCategoryChange(selected) {
      this.setState({ categorySelected:selected });
      console.log('User selected a cat');
    }

    render() {
        return (
            <section style={{marginTop:'10vh'}}>
                <Columns><Heading style={{textAlign:'center', fontSize:'3rem'}}></Heading>
                <Columns.Column size={3} offset={3}>
              </Columns.Column>
              </Columns>
              <Columns>
                  {this.state.articleCollection}
              </Columns>
          </section>
        )
    }
}

//Category Placeholder
// <Dropdown onChange={this.onCategoryChange} value={this.state.categorySelected} color="info">
//   {this.state.categoryCollection}
// </Dropdown>

//Search Placeholder
// <Columns.Column size={6} offset={3} style={{marginBottom:'10vh'}}>
//   <Box><Heading><input placeholder="Search" style={{width:'100%', border:'none', fontSize:'2rem'}} /></Heading></Box>
// </Columns.Column>
