import '../css/articles.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { Columns, Container, Section, Box, Tile, Heading, Image, Content, Card, Button } from 'react-bulma-components/full';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { } from '@fortawesome/fontawesome-svg-core';
import { faAngleDown, faAngleDoubleDown, faBack, faSearch, faCircle, faClock } from '@fortawesome/free-solid-svg-icons';

export class FeaturedPost extends React.Component {
    constructor() {
        super();
        this.state = { };
    }

    componentDidMount() {
      this.setBannerPost();
    }

    componentDidUpdate() {
      this.setBannerPost();
    }

    setBannerPost() {
      let stack = document.getElementById('articleMain');
      stack.innerHTML = this.props.focusedPostBody;
    }

    render() {
        return (
                <div className="blogContentContainer" style={{display:"none", backgroundImage: "url(" + this.props.featuredImage + ")"}}>
                  <section>
                    <Tile kind="parent">
                        <Tile renderAs="article" kind="child" size={12} >
                            <Heading subtitle className="titleCategory">
                              <span style={{background:'yellow'}}>{this.props.focusedPostDate}</span>
                              <span><b>Javascript</b></span>
                              <span><b>ES6</b></span>
                              <span><b>Webpack</b></span>
                            </Heading>
                            <Tile kind="parent">
                              <Tile renderAs="article" kind="child" notification className="blogBannerTile">
                                <Heading className="tileHeadingLarge"><span>{this.props.focusedPostTitle.toString()}</span></Heading>
                                <div id="articleMain" className="tileArticleMain"></div>
                              </Tile>
                            </Tile>
                      </Tile>
                    </Tile>
                  </section>
                  <Columns>
              </Columns>
            </div>
        )
    }
}
