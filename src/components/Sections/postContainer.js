var moment = require('moment');
import './css/welcome.css';
import React from 'react';
import ReactDOM from 'react-dom';
import {FeaturedPost} from './Blog/featuredPost';
import {PostFullList} from './Blog/postFullList';
import {Resume} from './resume';
import { Columns, Container, Tile, Heading, Image, Box } from 'react-bulma-components/full';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { } from '@fortawesome/fontawesome-svg-core';
import { faAngleDown, faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons'
let rawArr = [];
let wp_host = "https://grant-1a6bc8.easywp.com/wp-json/wp/v2";

export class PostContainer extends React.Component {
    constructor() {
        super();
        this.state = {dataLoaded:false}
        this.setMainPost = this.setMainPost.bind(this);
    }

    getPosts(categorySelected) {
      if(categorySelected) {
        console.log(categorySelected)
      } else {
        console.log('no category, geez all')
      }

      fetch(wp_host + '/posts?_embed')
        .then(response => { return response.json()})
        .then(posts => {
          //If no main image with post, set no image (or change to a default)
          let featuredImage = ''
          if(posts[0]._embedded['wp:featuredmedia']['0'].source_url == null) {
            // TODO: set a default based on category.
              featuredImage = "";
          } else {
            featuredImage = posts[0]._embedded['wp:featuredmedia']['0'].source_url;
          }

          this.setState({
             postTitle: posts[0].title.rendered,
             postBodySummary:posts[0].excerpt.rendered,
             postBodyMain:posts[0].content.rendered,
             postDate:moment(posts[0].date).format("Do MMM YYYY"),
             postCollection:posts,
             postImage:featuredImage,
          })
       })
       .then(()=> { this.getCategories();
     })
    }

    getCategories(url) {
      fetch(wp_host + '/categories')
        .then(response => { return response.json()})
        .then(categoryArr => {
          this.setState({
            postCategories:categoryArr,
            dataLoaded:true
          });
       })
    }

    setMainPost(id) {
      let posts = this.state.postCollection;
      for(let i = 0; i<posts.length; i++) {
        if(posts[i].id == id) {
          this.setState({
             postTitle: posts[i].title.rendered,
             postBodyMain:posts[i].content.rendered,
             postDate:moment(posts[i].date).format("Do MMM YYYY"),
             postImage:posts[i]._embedded['wp:featuredmedia']['0'].source_url,
             dataLoaded:true
          })
        }
        var element = document.getElementById("mainContentAnchor");
        element.scrollIntoView({ behavior: 'smooth', block: 'start' });
      }
    }

    componentWillMount() {
      this.getPosts();
    }

    render() {
      if(this.state.dataLoaded) {
        return (
            <section className="postSection">
            <FeaturedPost id="mainContentAnchor" focusedPostTitle={this.state.postTitle}
                  focusedPostBody={this.state.postBodyMain}
                  focusedPostDate={this.state.postDate}
                  featuredImage={this.state.postImage} />
            <PostFullList postCollection={this.state.postCollection}
                      postCategories={this.state.postCategories}
                      openPost={this.setMainPost} />
            </section>
        )
      } else {
        return (
          // TODO: Add a static loader to center the loading delay.
          <div style={{textAlign:'center'}}>Loading Posts.....</div>
      )}
    }
}
