import './css/welcome.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { Columns, Container, Tile, Heading, Image, Box } from 'react-bulma-components/full';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { } from '@fortawesome/fontawesome-svg-core';
import { faAngleDown, faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons'

export class Resume extends React.Component {
    constructor() {
        super();
        this.state = {}
    }

    render() {
        return (
            <div className="logoText" style={{marginTop:'10rem', marginBottom:'10rem', lineHeight:'1.2em', textAlign:'center', background:'rgba(31, 210, 210, 0.6)', padding:'3rem'}}>
              <p>I have a professional side</p>
              <p style={{marginTop:'10vh'}}><a href="https://www.gkd.design"><b>view my Resume</b></a>.</p>
            </div>
        )
    }
}
