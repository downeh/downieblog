import React from 'react';
import { Modal, Button } from 'react-bulma-components/full';


export class ContactModal extends React.Component {
    constructor() {
        super();
  this.state = {
    show: false,
  };
}

  open = () => this.setState({ show: true });
  close = () => this.setState({ show: false });

  render() {
    return (
      <div style={{float:'left', marginLeft:'1rem'}}>
        <Button  onClick={this.open} color="info" size="medium">Contact</Button>
        <Modal show={this.state.show} onClose={this.close} {...this.props.modal}>
          Hellooooo
        </Modal>
      </div>
    );
  }
}