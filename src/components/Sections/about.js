import React from 'react';
import ReactDOM from 'react-dom';
import './css/about.css';
import { Section, Hero, Columns, Footer, Container, Content, Heading, Button, Modal } from 'react-bulma-components/full';
import mainAv from'../../../images/av.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInstagram, faLinkedinIn, faBitbucket } from '@fortawesome/free-brands-svg-icons';

export class About extends React.Component {
    constructor() {
        super();
        this.state = {}
    }

    render() {
        return (
                    <Content style={{ position: 'relative', background: '#fafafa', border:'1px solid #eee', marginTop:'20vh'}}>
                      <Container>
                        <Section>
                          <div id="About" className="aboutContainer">
                            <Columns>
                            <Columns.Column>
                              <div className="grantAv"><img src={mainAv} alt="Grant Face" /></div>
                            </Columns.Column>
                            <Columns.Column>
                              <Heading size={2}>As you guessed, my name is Grant.</Heading>
                              <p>I've over 10 years experience working within large multinationals within the IT and financial sector.</p>
                              <p>But one consistent theme amongst these is my love for web development. Call me stubborn but i've shaped
                                each of these roles to align with my personal interests - web design and all things creative.</p>
                              <div style={{textAlign:'center', marginTop:'5rem'}}>
                                <Button color="warning" size="medium" onClick={() => { window.open('https://www.gkd.design') }}>View Professional Resume</Button>
                              </div>
                            </Columns.Column>
                            </Columns>
                            <Section>
                              <Columns>
                                <Columns.Column>
                                    <Heading size={3}>What do I <i><b>Really</b></i> enjoy working on?</Heading>
                                </Columns.Column>
                                <Columns.Column>
                                  <ul style={{listStyle:'none', textAlign:'center'}}>
                                    <li>Wireframing</li>
                                    <li>Presenting</li>
                                    <li>Collaboration</li>
                                  </ul>
                                </Columns.Column>
                                <Columns.Column>
                                  <ul style={{listStyle:'none', textAlign:'center'}}>
                                    <li>Data Analytics</li>
                                    <li>Documentation</li>
                                    <li>Testing</li>
                                  </ul>
                                </Columns.Column>
                                <Columns.Column>
                                  <ul style={{listStyle:'none', textAlign:'center'}}>
                                    <li>Visualisations</li>
                                    <li>CSS Animation</li>
                                    <li>Responsive Design</li>
                                  </ul>
                                </Columns.Column>
                              </Columns>
                            </Section>
                          </div>
                        </Section>
                      </Container>
                    </Content>
        )
    }
}