import React from 'react';
import ReactDOM from 'react-dom';
import './css/portfolio.css';
import { Section, Hero, Columns, Footer, Container, Content, Heading, Button, Card } from 'react-bulma-components/full';
import Image1 from'../../../images/portfolio/1.jpg';
import Image2 from'../../../images/portfolio/2.jpg';
import Image3 from'../../../images/portfolio/3.jpg';
import Image4 from'../../../images/portfolio/4.jpg';

export class Portfolio extends React.Component {
    constructor() {
        super();
        this.state = {}
    }

    render() {
        return (
                    <Content>
                      <Container>
                        <Section>
                          <Columns>
                            <Columns.Column>
                            <Heading size={2}>My Work</Heading>
                            </Columns.Column>
                          </Columns>
                          <Columns>
                            <Columns.Column size={9} id="Work">
                            <div className="portfolioItem">
                              <img src={Image1} alt="Image 1" />
                              <div className="hover-content positionBottom" style={{background:'rgb(207, 214, 220)'}}>
                                <a><Heading size={3}>Financial Risk management suite for daily risk reporting</Heading></a>
                              </div>
                            </div>
                            </Columns.Column>
                          </Columns>

                          <Columns>
                            <Columns.Column size={9} offset={3}>
                            <div className="portfolioItem">
                              <img src={Image3} alt="Image 1" />
                              <div className="hover-content positionBottom" style={{background:'rgb(29, 24, 21)'}}>
                                <a><Heading size={3} style={{color:'white'}}>Environment monitoring of IQ and ASE environments</Heading></a>
                              </div>
                            </div>
                            </Columns.Column>
                            </Columns>

                            <Columns>
                            <Columns.Column size={9}>
                            <div className="portfolioItem">
                              <img src={Image4} alt="Image 1" />
                              <div className="hover-content positionBottom" style={{background:'rgb(52, 93, 85)'}}>
                                <a><Heading size={3} style={{color:'white'}}>Multi-search engine crawler with categorised results</Heading></a>
                              </div>
                            </div>
                            </Columns.Column>
                          </Columns>
                        </Section>
                      </Container>
                    </Content>
        )
    }
}