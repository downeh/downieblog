import './header.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { Navbar, Icon, Tag, Container, Box, Columns } from 'react-bulma-components/full';
import { bars } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInstagram, faLinkedinIn, faBitbucket } from '@fortawesome/free-brands-svg-icons';

export class Header extends React.Component {
    constructor() {
        super();
        this.state = { isHide: true }
    }

    hideBar = () => {
        const { isHide } = this.state
 
        window.scrollY > 0 ?
        isHide && this.setState({ isHide: false })
        :
        !isHide && this.setState({ isHide: true });
     }
 
     componentDidMount(){
         window.addEventListener('scroll', this.hideBar);
     }
 
     componentWillUnmount(){
          window.removeEventListener('scroll', this.hideBar);
     }

     scrollToID(e) {
     document.getElementById(e.target.innerHTML).scrollIntoView({
        behavior: 'smooth'
      });
    }

     
    render() {
        const classHide = this.state.isHide ? 'hide' : '';
        return (
              <Navbar className={`${classHide}`}>
                <Navbar.Item href="/">GRANT<b>DOWNIE</b></Navbar.Item>
                <Navbar.Container position="end" className="leftFloat">
                  <Navbar.Item onClick={(e) => { this.scrollToID(e) }} className="leftFloat">About</Navbar.Item>
                  <Navbar.Item onClick={(e) => { this.scrollToID(e) }} className="leftFloat">Work</Navbar.Item>
                  {/* <Navbar.Item onClick={(e) => { this.scrollToID(e) }} className="leftFloat">Contact</Navbar.Item> */}
                  <Navbar.Item onClick={(e) => { this.scrollToID(e) }} className="leftFloat">Posts</Navbar.Item>
                </Navbar.Container>
              </Navbar>
        )
    }
}
