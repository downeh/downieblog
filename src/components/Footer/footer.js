import './footer.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { Hero, Footer, Container, Content } from 'react-bulma-components/full';

export class FooterContainer extends React.Component {
    constructor() {
        super();
        this.state = {}
    }

    render() {
        return (
                <Footer style={{marginTop:'10vh', background:'white'}}>
                  <Container>
                    <Content style={{ textAlign: 'center' }}>
                      <p><strong>GKD</strong> /<a href="https://www.gkd.design">resume</a></p>
                    </Content>
                  </Container>
                </Footer>
        )
    }
}
