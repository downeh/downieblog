import React from 'react';
import ReactDOM from 'react-dom';
import {Header} from './components/Header/header';
import {Welcome} from './components/Sections/welcome';
import {About} from './components/Sections/about';
import {Portfolio} from './components/Sections/portfolio';
import {Contact} from './components/Sections/contact';
import {PostContainer} from './components/Sections/postContainer'
import {FooterContainer} from './components/Footer/footer';
import { Tile, Heading, Container, Box, Button, Columns } from 'react-bulma-components/full';

export class Main extends React.Component {
    constructor() {
        super()
        this.state = { visibleSection: null }
        this.changeSection = this.changeSection.bind(this);
      }

      changeSection(section) {
        this.setState({visibleSection: <Blog changeSection={this.changeSection} />})
      }

      componentWillMount() {
        this.setState({visibleSection: <Welcome changeSection={this.changeSection} />})
      }

    render() {
        return (
              <div>
                <Header />
                <Welcome />
                <About />
                <Portfolio />
                <FooterContainer />
              </div>
        )
    }
}


              //  <PostContainer />
